import http.server
import ssl


def main():
    httpd = http.server.HTTPServer(
        ('localhost', 443),
        http.server.SimpleHTTPRequestHandler
    )
    httpd.socket = ssl.wrap_socket(
        httpd.socket,
        certfile='./certificate.pem',
        server_side=True,
        ssl_version=ssl.PROTOCOL_TLS
    )
    httpd.serve_forever()


main()

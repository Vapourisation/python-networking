# TLS implementation

## Important notes
Things that _may be_ required to get this to work properly:

### You'll need some certs

This requires `openssl`, so if you don't have that, install it.

#### Creating the private key
```bash
openssl genrsa -out key.pem 2048

openssl genrsa -aes256 -out key.pem 2048
```

#### Creating the CSR using the key
```bash
openssl req -new -key key.pem -out signreq.csr
```

#### Sign the cert
```bash
openssl x509 -req -days 365 -in signreq.csr -signkey key.pem -out certificate.pem
```
You can check this cert details by running:

```bash
openssl x509 -text -noout -in certificate.pem
```


class TLSServer:
    def __init__(self, data: dict):
        self.accepted_tls_versions = data.get('tls_versions', [1.3,])
        self.accepted_cipher_suites = data.get('accepted_cipher_suites', [])
        self.cert = data.get('cert', './certs/main.crt')
        pass

    def receive(self, data: dict):
        # Used to process incoming data. Will be one of the TLS steps
        pass

    def _ack(self):
        pass

    def _process_hello(self):
        pass

    def _generate_random(self):
        pass

    def _generate_session_keys(self):
        pass

    def _key_exchange(self):
        pass

    def _change_cipher_specification(self):
        pass

    def _success(self):
        pass

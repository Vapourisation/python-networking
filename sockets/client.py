# Import socket module
import socket

def udp():
    target_host = "127.0.0.1"
    target_port = 9997
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    client.sendto(b"AAABBBCC",(target_host, target_port))
    data, addr = client.recvfrom(4096)
    print(data.decode())
    client.close()

def tcp():
    target_host = "0.0.0.0"
    target_port = 9998
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((target_host, target_port))
    client.send(b"GET / HTTP/1.1\r\nHost: google.com\r\n\r\n")
    response = client.recv(4096)
    print(response.decode())
    client.close()


if __name__ == '__main__':
    tcp()
